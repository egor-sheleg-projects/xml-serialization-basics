﻿using System.Xml.Serialization;

#pragma warning disable CA1819

namespace XmlSerializationBasics.Sequence
{
    [XmlRootAttribute("book-shop-item", Namespace = "http://contoso.com/book-shop-item")]
    public class BookInfo
    {
        [XmlElementAttribute("title", Order = 3)]
        public string[] Titles { get; set; }

        [XmlElementAttribute("price", Order = 4)]
        public decimal[] Prices { get; set; }

        [XmlElementAttribute("genre", Order = 1)]
        public string[] Genres { get; set; }

        [XmlElementAttribute("international-standard-book-number", Order = 2)]
        public string[] Codes { get; set; }

        [XmlElementAttribute("publication-date", Order = 5)]
        public string[] PublicationDates { get; set; }
    }
}
