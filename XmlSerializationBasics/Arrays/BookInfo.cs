﻿using System.Xml.Serialization;

#pragma warning disable CA1819

namespace XmlSerializationBasics.Arrays
{
    [XmlRootAttribute("book-shop-item", Namespace = "http://contoso.com/book-shop-item")]
    public class BookInfo
    {
        [XmlArrayAttribute("titles", Order = 1), XmlArrayItemAttribute("title")]
        public string[] Titles { get; set; }

        [XmlArrayAttribute("prices", Order = 5), XmlArrayItemAttribute("price")]
        public decimal[] Prices { get; set; }

        [XmlArrayAttribute("genres", Order = 2), XmlArrayItemAttribute("genre")]
        public string[] Genres { get; set; }

        [XmlArrayAttribute("international-standard-book-numbers", Order = 4), XmlArrayItemAttribute("international-standard-book-number")]
        public string[] Codes { get; set; }

        [XmlArrayAttribute("publication-dates", Order = 3), XmlArrayItemAttribute("publication-date")]
        public string[] PublicationDates { get; set; }
    }
}
